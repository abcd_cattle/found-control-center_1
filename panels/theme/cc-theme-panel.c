/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2017 Mohammed Sadiq <sadiq@sadiqpk.org>
 * Copyright (C) 2010 Red Hat, Inc
 * Copyright (C) 2008 William Jon McCann <jmccann@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n.h>

#include <stdio.h>
#include <dirent.h>

#include "cc-theme-panel.h"
#include "cc-theme-resources.h"

/* Autorun options */
#define GTK_THEME                 "gtk-theme"
#define ICON_THEME                "icon-theme"
#define CURSOR_THEME              "cursor-theme"

#define GNOME_DESKTOP_INTERFACE_SCHEMA    "org.gnome.desktop.interface"

#define INTERFACE_SCHEMA          "org.cdos.desktop.interface"

struct _CcThemePanel
{
  CcPanel              parent_instance;

  GtkWidget *          gtk_theme_combox;
  GtkWidget *          icon_theme_combox;
  GtkWidget *          cursor_theme_combox;

  GSettings           *settings;
  GSettings           *gnome_settings;
};

G_DEFINE_TYPE (CcThemePanel, cc_theme_panel, CC_TYPE_PANEL)

static void
ellipsize_cell_layout (GtkCellLayout *cell_layout)
{
  GList *cells = gtk_cell_layout_get_cells (cell_layout);
  GList *cell;

  for (cell = cells; cell; cell = cell->next)
    if (GTK_IS_CELL_RENDERER_TEXT (cell->data))
      g_object_set (G_OBJECT (cell->data), "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  g_list_free (cells);
}

static struct {
	const char* set_string;
	const char* displayname;
} gtk_themes[] = {
	{"Panda",  "经典主题"},
	{"DarkPanda",  "暗色主题"}
};

static struct {
	const char* set_string;
	const char* displayname;
} icon_themes[] = {
	{"Panda",  "时尚"},
	{"DarkPanda",  "经典暗色"}
};

static struct {
	const char* set_string;
	const char* displayname;
} cursor_themes[] = {
	{"Adwaita",  "炫酷黑"},
	{"mate",  "灵动白"}
};

static void
gtk_theme_changed (GtkComboBox *widget,
               CcThemePanel * self)
{
  GtkTreeIter   iter;
  gchar        *string = NULL;
  GtkTreeModel *model;

  /* Obtain currently selected item from combo box.
    * If nothing is selected, do nothing. */
  if ( gtk_combo_box_get_active_iter ( widget, &iter ) )
  {
      /* Obtain data model from combo box. */
      model = gtk_combo_box_get_model ( widget );

      /* Obtain string from model. */
      gtk_tree_model_get ( model, &iter, 0, &string, -1 );
  }

  if (self->settings != NULL) 
  {
    gint i;
    for (i = 0; i < G_N_ELEMENTS(gtk_themes); i++)
    {
      if (g_str_equal (string, gtk_themes[i].displayname))
      {
          g_settings_set_string (self->settings,
                       GTK_THEME, gtk_themes[i].set_string);
      }
    }
  }
  /* Free string (if not NULL). */
  if ( string )
      g_free( string );
  
}

static void
icon_theme_changed (GtkComboBox *widget,
               CcThemePanel * self)
{

  GtkTreeIter   iter;
  gchar        *string = NULL;
  GtkTreeModel *model;

  /* Obtain currently selected item from combo box.
    * If nothing is selected, do nothing. */
  if ( gtk_combo_box_get_active_iter ( widget, &iter ) )
  {
      /* Obtain data model from combo box. */
      model = gtk_combo_box_get_model ( widget );

      /* Obtain string from model. */
      gtk_tree_model_get ( model, &iter, 0, &string, -1 );
  }

  if (self->settings != NULL) 
  {
    gint i;
    for (i = 0; i < G_N_ELEMENTS(icon_themes); i++)
    {
      if (g_str_equal (string, icon_themes[i].displayname))
      {
          g_settings_set_string (self->settings,
                       ICON_THEME, icon_themes[i].set_string);
      }
    }
  }
  /* Free string (if not NULL). */
  if ( string )
      g_free ( string );
}

static void
cursor_theme_changed (GtkComboBox *widget,
               CcThemePanel * self)
{
  GtkTreeIter   iter;
  gchar        *string = NULL;
  GtkTreeModel *model;

  /* Obtain currently selected item from combo box.
    * If nothing is selected, do nothing. */
  if ( gtk_combo_box_get_active_iter ( widget, &iter ) )
  {
      /* Obtain data model from combo box. */
      model = gtk_combo_box_get_model ( widget );

      /* Obtain string from model. */
      gtk_tree_model_get ( model, &iter, 0, &string, -1 );
  }

  if (self->gnome_settings != NULL) 
  {
    gint i;
    for (i = 0; i < G_N_ELEMENTS(cursor_themes); i++)
    {
      if (g_str_equal (string, cursor_themes[i].displayname))
      {
          g_settings_set_string (self->gnome_settings,
                       CURSOR_THEME, cursor_themes[i].set_string);
      }
    }
  }
  /* Free string (if not NULL). */
  if ( string )
      g_free ( string );
  
}

static void
create_combobox_with_model (CcThemePanel   *self, 
                            GtkWidget      *combobox,
                            gchar          *theme_string)
{
    GtkListStore *store = NULL;
    GtkTreeIter iter;
    GtkCellRenderer *renderer = NULL;
    gchar *theme;
    gboolean in_themes = FALSE;

    store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
    gtk_combo_box_set_model (combobox, GTK_TREE_MODEL(store));
    if (strcmp (GTK_THEME, theme_string) == 0)
    {
      theme = g_settings_get_string (self->settings, theme_string);
      gint i;
      for (i = 0; i < G_N_ELEMENTS(gtk_themes); i++)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, gtk_themes[i].displayname,
                                          1, gtk_themes[i].set_string, -1);

        if (strcmp (gtk_themes[i].set_string, theme) == 0)
        {
          gtk_combo_box_set_active_iter (combobox, &iter);
          in_themes = TRUE;
        }
      }
      if (in_themes == FALSE)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, theme, 1, theme, -1);
        gtk_combo_box_set_active_iter (combobox, &iter);
      }
    }
    else if (strcmp (ICON_THEME, theme_string) == 0)
    {
      theme = g_settings_get_string (self->settings, theme_string);
      gint i;
      for (i = 0; i < G_N_ELEMENTS(icon_themes); i++)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, icon_themes[i].displayname,
                                          1, icon_themes[i].set_string, -1);

        if (strcmp (icon_themes[i].set_string, theme) == 0)
        {
          gtk_combo_box_set_active_iter (combobox, &iter);
          in_themes = TRUE;
        }
      }
      if (in_themes == FALSE)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, theme, 1, theme, -1);
        gtk_combo_box_set_active_iter (combobox, &iter);
      }
    }
    else if (strcmp (CURSOR_THEME, theme_string) == 0)
    {
      theme = g_settings_get_string (self->gnome_settings, theme_string);
      gint i;
      for (i = 0; i < G_N_ELEMENTS(cursor_themes); i++)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, cursor_themes[i].displayname,
                                          1, cursor_themes[i].set_string, -1);

        if (strcmp (cursor_themes[i].set_string, theme) == 0)
        {
          gtk_combo_box_set_active_iter (combobox, &iter);
          in_themes = TRUE;
        }
      }
      if (in_themes == FALSE)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, theme, 1, theme, -1);
        gtk_combo_box_set_active_iter (combobox, &iter);
      }
    }
    
    // 设置GtkTreeModel中的每一项数据如何在列表框中显示
    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT(combobox), renderer, TRUE);
    gtk_cell_layout_set_attributes ( GTK_CELL_LAYOUT (combobox), renderer, "text", 0, NULL);
    g_free (theme);
}

static void
info_panel_setup_media (CcThemePanel *self)
{
  create_combobox_with_model (self, self->gtk_theme_combox, GTK_THEME);
  create_combobox_with_model (self, self->icon_theme_combox, ICON_THEME);
  create_combobox_with_model (self, self->cursor_theme_combox, CURSOR_THEME);

  g_signal_connect (self->gtk_theme_combox, 
                    "changed", 
                    G_CALLBACK(gtk_theme_changed), 
                    self);
  g_signal_connect (self->icon_theme_combox, 
                    "changed", 
                    G_CALLBACK(icon_theme_changed), 
                    self);
  g_signal_connect (self->cursor_theme_combox, 
                    "changed", 
                    G_CALLBACK(cursor_theme_changed), 
                    self);
}

static void
cc_theme_panel_finalize (GObject *object)
{
  CcThemePanel *self = CC_THEME_PANEL (object);

  G_OBJECT_CLASS (cc_theme_panel_parent_class)->finalize (object);
}

static void
theme_restore_default_settings (CcThemePanel *self)
{
  g_settings_reset(self->settings, GTK_THEME);
  g_settings_reset(self->settings, ICON_THEME);
  g_settings_reset(self->gnome_settings, CURSOR_THEME);
}

static void
set_active_iter(GSettings     *settings,
                 const gchar  *key, 
                 GtkWidget    *combobox)
{
  GtkTreeIter   iter;
  GtkTreeModel *model;
  gboolean      valid;
  gchar *theme_value = NULL;
  gchar *model_value = NULL;

  theme_value = g_settings_get_string (settings, key);
  if (theme_value)
  {
    model = gtk_combo_box_get_model (combobox);
    for (valid = gtk_tree_model_get_iter_first (model, &iter);
         valid;
         valid = gtk_tree_model_iter_next (model, &iter))
    {
      gtk_tree_model_get (model, &iter, 1, &model_value, -1);
      if (g_str_equal (theme_value, model_value))
      {
        gtk_combo_box_set_active_iter (combobox, &iter);
        break;
      }
    }  
  }
}

static void 
settings_update_active (GSettings     *settings,
                        const gchar   *key,
                        CcThemePanel  *self)
{
  if (g_str_equal (key, GTK_THEME))
    set_active_iter (settings, key, self->gtk_theme_combox);
  else if (g_str_equal (key, ICON_THEME))
    set_active_iter (settings, key, self->icon_theme_combox);
}

static void 
gnome_settings_update_active(GSettings  *settings, 
                            const gchar *key, 
                            CcThemePanel *self)
{
  if (g_str_equal (key, CURSOR_THEME))
    set_active_iter (settings, key, self->cursor_theme_combox);
}

static void
cc_theme_panel_dispose (GObject *object)
{
  CcThemePanel *self = CC_THEME_PANEL (object);
  g_clear_object (&self->settings);
  g_clear_object (&self->gnome_settings);
  G_OBJECT_CLASS (cc_theme_panel_parent_class)->dispose (object);
}

static void
cc_theme_panel_class_init (CcThemePanelClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = cc_theme_panel_finalize;
  object_class->dispose = cc_theme_panel_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/control-center/theme/cc-theme-panel.ui");

  gtk_widget_class_bind_template_child (widget_class, CcThemePanel, gtk_theme_combox);
  gtk_widget_class_bind_template_child (widget_class, CcThemePanel, icon_theme_combox);
  gtk_widget_class_bind_template_child (widget_class, CcThemePanel, cursor_theme_combox);
  gtk_widget_class_bind_template_callback (widget_class, theme_restore_default_settings);

}

static void
cc_theme_panel_init (CcThemePanel *self)
{
  g_resources_register (cc_theme_get_resource ());
  gtk_widget_init_template (GTK_WIDGET (self));
  self->settings = g_settings_new (INTERFACE_SCHEMA);
  self->gnome_settings = g_settings_new (GNOME_DESKTOP_INTERFACE_SCHEMA);

  ellipsize_cell_layout (GTK_CELL_LAYOUT (self->gtk_theme_combox));
  ellipsize_cell_layout (GTK_CELL_LAYOUT (self->icon_theme_combox));
  ellipsize_cell_layout (GTK_CELL_LAYOUT (self->cursor_theme_combox));
  
  info_panel_setup_media (self);
  g_signal_connect (self->settings, "changed",
                                G_CALLBACK (settings_update_active), self);
  g_signal_connect (self->gnome_settings, "changed",
                                G_CALLBACK (gnome_settings_update_active), self);
}
