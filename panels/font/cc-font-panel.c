/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2017 Mohammed Sadiq <sadiq@sadiqpk.org>
 * Copyright (C) 2010 Red Hat, Inc
 * Copyright (C) 2008 William Jon McCann <jmccann@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n.h>

#include "cc-font-panel.h"
#include "cc-font-resources.h"

/* font options */
#define FONT_NAME                "font-name"
#define MONOSPACE_FONT_NAME      "monospace-font-name"
#define TEXT_SCALING_FACTOR      "text-scaling-factor"

#define ANTIALIASING             "antialiasing"
#define HINTING                  "hinting"

#define CDOS_INTERFACE_SCHEMA    "org.cdos.desktop.interface"
#define XSETTINGS_SCHEMA         "org.cdos.settings-daemon.plugins.xsettings"


struct _CcFontPanel
{
  CcPanel              parent_instance;

  GtkFontButton *default_font;
  GtkFontButton *equal_font;
  GtkSpinButton *zoom_ratio;

  GtkWidget *smooth;
  GtkWidget *fine_tuning;

  GSettings *cdos_settings;
  GSettings *xset_settings;
};


G_DEFINE_TYPE (CcFontPanel, cc_font_panel, CC_TYPE_PANEL)

static void
default_font_changed (GtkFontButton *button, CcFontPanel * self) 
{

  gchar * select_font = gtk_font_chooser_get_font (button);
  if (self->cdos_settings != NULL) 
  {
    g_settings_set_string (self->cdos_settings,
                         FONT_NAME, select_font);
  }

  g_free(select_font);
}

static void
equal_font_changed (GtkFontButton *button, CcFontPanel * self) 
{
  gchar * select_font = gtk_font_chooser_get_font (button);
  if (self->cdos_settings != NULL) 
  {
    g_settings_set_string (self->cdos_settings,
                       MONOSPACE_FONT_NAME, select_font);
  }
  g_free(select_font);
}

static void
zoom_ratio_changed (GtkSpinButton *spin_button,
               CcFontPanel * self)
{
  gdouble value = gtk_spin_button_get_value(spin_button);
  if (self->cdos_settings != NULL) 
  {
    g_settings_set_double (self->cdos_settings,
                       TEXT_SCALING_FACTOR, value);
  }
}

static void
smooth_changed (GtkComboBox *widget,
               CcFontPanel * self)
{
  if (self->xset_settings != NULL) 
  {
    g_settings_set_enum (self->xset_settings,
                       ANTIALIASING, gtk_combo_box_get_active (widget));
  }
}

static void
fine_tuning_changed (GtkComboBox *widget,
               CcFontPanel * self)
{

  if (self->xset_settings != NULL) 
  {
    g_settings_set_enum (self->xset_settings,
                       HINTING, gtk_combo_box_get_active (widget));
  }
  
}

const char * const smooth_types[] = {
      "none",
      "Grayscale",
      "Rgba",
      NULL
    };

    const char * const fine_tuning_types[] = {
      "none",
      "Slight",
      "Medium",
      "Full",
      NULL
    };

static void
create_combobox_with_model (CcFontPanel *self, GtkWidget *combobox, gchar* font_set)
{
    GtkListStore *store = NULL;
    GtkTreeIter iter;
    GtkCellRenderer *renderer = NULL;
    gchar *font_name;
    gint select_type;
    
    

    // 设置GtkTreeModel中的每一项数据如何在列表框中显示
    store = gtk_list_store_new (1, G_TYPE_STRING);
    gtk_combo_box_set_model (combobox, GTK_TREE_MODEL(store));
    g_object_set_data_full (combobox, "model", store, g_object_unref);

    select_type = g_settings_get_enum (self->xset_settings, font_set);
    if (strcmp (ANTIALIASING, font_set) == 0)
    {
      guint i;
	    for (i = 0; smooth_types[i]; i++)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, _(smooth_types[i]), -1);
        if (i == select_type)
        {
          gtk_combo_box_set_active_iter (combobox, &iter);
        }
      }
    }
    else if (strcmp (HINTING, font_set) == 0)
    {
      guint i;
	    for (i = 0; fine_tuning_types[i]; i++)
      {
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter, 0, _(fine_tuning_types[i]), -1);
        if (i == select_type)
        {
          gtk_combo_box_set_active_iter (combobox, &iter);
        }
      }
    }
    
    // 设置GtkTreeModel中的每一项数据如何在列表框中显示
    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT(combobox), renderer, TRUE);
    gtk_cell_layout_set_attributes ( GTK_CELL_LAYOUT (combobox), renderer, "text", 0, NULL);
    g_free (font_name);
}

static void
font_button_set_cdos_font_name (CcFontPanel *self, GtkWidget *button, const gchar *key)
{
  gchar *font_name;
  font_name = g_settings_get_string (self->cdos_settings, key);
  gtk_font_button_set_font_name (button, font_name);
  g_free (font_name);
}

static void
font_button_set_gnome_font_name (CcFontPanel *self, GtkWidget *button, const gchar *key)
{
  gchar *font_name;
  font_name = g_settings_get_string (self->cdos_settings, key);
  gtk_font_button_set_font_name (button, font_name);
  g_free (font_name);
}

static void
info_panel_setup_media (CcFontPanel *self)
{
  GtkAdjustment  *adjustment;
  adjustment = gtk_adjustment_new(1.000, 0.5, 3.0, 0.01, 0.0, 0.0);
  gtk_spin_button_set_adjustment (self->zoom_ratio, adjustment);

  font_button_set_cdos_font_name (self, self->default_font, FONT_NAME);
  font_button_set_gnome_font_name (self, self->equal_font, MONOSPACE_FONT_NAME);

  create_combobox_with_model (self, self->smooth, ANTIALIASING);
  create_combobox_with_model (self, self->fine_tuning, HINTING);

  g_signal_connect (self->default_font, 
                    "font_set", 
                    G_CALLBACK(default_font_changed), 
                    self);
  g_signal_connect (self->equal_font, 
                    "font_set", 
                    G_CALLBACK(equal_font_changed), 
                    self);
  g_signal_connect (self->zoom_ratio, 
                    "value_changed", 
                    G_CALLBACK(zoom_ratio_changed), 
                    self);
  g_signal_connect (self->smooth, 
                    "changed", 
                    G_CALLBACK(smooth_changed), 
                    self);
  g_signal_connect (self->fine_tuning, 
                    "changed", 
                    G_CALLBACK(fine_tuning_changed), 
                    self);
}

static void
cc_font_panel_finalize (GObject *object)
{
  CcFontPanel *self = CC_FONT_PANEL (object);
  
  G_OBJECT_CLASS (cc_font_panel_parent_class)->finalize (object);
}

static void
cc_font_panel_dispose (GObject *object)
{
  CcFontPanel *self = CC_FONT_PANEL (object);

  g_clear_object (&self->cdos_settings);

  g_clear_object (&self->xset_settings);

  G_OBJECT_CLASS (cc_font_panel_parent_class)->dispose (object);
}

static void
font_restore_default_settings (CcFontPanel *self)
{
  g_settings_reset (self->cdos_settings, FONT_NAME);
  g_settings_reset (self->cdos_settings, MONOSPACE_FONT_NAME);
  g_settings_reset (self->cdos_settings, TEXT_SCALING_FACTOR);
  g_settings_reset (self->xset_settings, ANTIALIASING);
  g_settings_reset (self->xset_settings, HINTING);
}

static void
cdos_update_active(GSettings   *settings,
                  const gchar  *key,
                  CcFontPanel  *self)
{
  gchar *font = NULL;
  if (g_str_equal (key, FONT_NAME))
  {
    font = g_settings_get_string (settings, key);
    gtk_font_chooser_set_font (self->default_font, font);
    g_free (font);
  }
  else if (g_str_equal (key, MONOSPACE_FONT_NAME))
  {
    font = g_settings_get_string (settings, key);
    gtk_font_chooser_set_font (self->equal_font, font);
    g_free (font);
  }
  else if (g_str_equal (key, TEXT_SCALING_FACTOR) )
  {
    gdouble value =  g_settings_get_double (settings, key);
    gtk_spin_button_set_value (self->zoom_ratio,  value);
  }
}

static void
xset_update_active(GSettings   *settings,
                  const gchar  *key,
                  CcFontPanel  *self)
{
  gint value;
  if (g_str_equal (key,  ANTIALIASING))
  {
      value = g_settings_get_enum (settings, key);
      gtk_combo_box_set_active (self->smooth, value);
  }
  else if (g_str_equal (key, HINTING))
  {
      value = g_settings_get_enum (settings, key);
      gtk_combo_box_set_active (self->fine_tuning, value);
  }
}

static void
load_custom_css (CcFontPanel *self)
{
  g_autoptr(GtkCssProvider) provider = NULL;

  /* use custom CSS */
  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, "/org/gnome/control-center/font/font-panel.css");
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

static void
cc_font_panel_class_init (CcFontPanelClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = cc_font_panel_finalize;
  object_class->dispose = cc_font_panel_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/control-center/font/cc-font-panel.ui");

  gtk_widget_class_bind_template_child (widget_class, CcFontPanel, default_font);
  gtk_widget_class_bind_template_child (widget_class, CcFontPanel, equal_font);
  /*
  gtk_widget_class_bind_template_child (widget_class, CcFontPanel, zoom_ratio);

  gtk_widget_class_bind_template_child (widget_class, CcFontPanel, smooth);
  gtk_widget_class_bind_template_child (widget_class, CcFontPanel, fine_tuning);
  */
  gtk_widget_class_bind_template_callback (widget_class, font_restore_default_settings);

}

static void
cc_font_panel_init (CcFontPanel *self)
{
  g_resources_register (cc_font_get_resource ());
  load_custom_css (self);

  gtk_widget_init_template (GTK_WIDGET (self));

  self->cdos_settings  = g_settings_new (CDOS_INTERFACE_SCHEMA);
  self->xset_settings  = g_settings_new (XSETTINGS_SCHEMA);

  info_panel_setup_media (self);
  g_signal_connect(self->cdos_settings, "changed",
                              G_CALLBACK(cdos_update_active ), self);
  g_signal_connect(self->xset_settings, "changed",
                              G_CALLBACK(xset_update_active ), self);
  gtk_widget_set_size_request (self->default_font, 300, 50);
  gtk_widget_set_size_request (self->equal_font, 300, 50);

}
