/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2019 Purism SPC
 * Copyright (C) 2017 Mohammed Sadiq <sadiq@sadiqpk.org>
 * Copyright (C) 2010 Red Hat, Inc
 * Copyright (C) 2008 William Jon McCann <jmccann@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n.h>

#include "cc-info-protocol-panel.h"
#include "cc-info-protocol-resources.h"

struct _CcInfoProtocolPanel
{
  CcPanel          parent_instance;
  
  GtkTextBuffer   *textbuffer1;
};

G_DEFINE_TYPE (CcInfoProtocolPanel, cc_info_protocol_panel, CC_TYPE_PANEL)

static void
cc_info_protocol_panel_finalize (GObject *object)
{
  CcInfoProtocolPanel *self = CC_INFO_PROTOCOL_PANEL (object);
  
  G_OBJECT_CLASS (cc_info_protocol_panel_parent_class)->finalize (object);
}

static void
cc_info_protocol_panel_dispose (GObject *object)
{
  CcInfoProtocolPanel *self = CC_INFO_PROTOCOL_PANEL (object);

  G_OBJECT_CLASS (cc_info_protocol_panel_parent_class)->dispose (object);
}

static void
cc_info_protocol_panel_class_init (CcInfoProtocolPanelClass *klass)
{
  GObjectClass    *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass  *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = cc_info_protocol_panel_finalize;
  object_class->dispose = cc_info_protocol_panel_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/control-center/info-protocol/cc-info-protocol-panel.ui");

  gtk_widget_class_bind_template_child (widget_class, CcInfoProtocolPanel, textbuffer1);
}

static void
set_center_line (CcInfoProtocolPanel *self, gint line_number)
{
  GtkTextIter       start_iter;
  GtkTextIter       end_iter;
  GtkTextTag       *tag;

  gtk_text_buffer_get_iter_at_line (self->textbuffer1, 
                                    &start_iter, 
                                    line_number - 1);

  gtk_text_buffer_get_iter_at_line_offset (self->textbuffer1, 
                                           &end_iter,
                                           line_number - 1,
                                           gtk_text_iter_get_chars_in_line (&start_iter) - 1);

  tag = gtk_text_buffer_create_tag (self->textbuffer1,
                                    NULL, 
                                    "justification", 
                                    GTK_JUSTIFY_CENTER, 
                                    NULL);
                                    
  gtk_text_buffer_apply_tag (self->textbuffer1, tag, &start_iter, &end_iter);
}

static void
cc_info_protocol_panel_init (CcInfoProtocolPanel *self)
{
  g_resources_register (cc_info_protocol_get_resource ());
  gtk_widget_init_template (GTK_WIDGET (self));

  gchar       *license;
  // GBytes      *bytes;
  // GtkTextIter  iter;

  // g_resources_register (g_resource_load ("/usr/share/licenses/licenses.gresource", NULL));
  // bytes = g_resources_lookup_data ("/org/desktop/eula/eula_zh_CN.txt", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  // if (bytes != NULL)
  // {
  //   license = g_bytes_get_data (bytes, NULL);
  //   gtk_text_buffer_get_start_iter (self->textbuffer1, &iter);
  //   gtk_text_buffer_insert_markup (self->textbuffer1, &iter, license, -1);
  //   set_center_line (self, 2);
  //   set_center_line (self, 3);
  // }

  if (0 == access ("/usr/share/licenses/eula_zh_CN.txt", F_OK))
  {
    FILE *stream = fopen ("/usr/share/licenses/eula_zh_CN.txt", "r");
    fseek (stream, 0, SEEK_END);
    long size = ftell (stream);
    license = (gchar*) malloc (size + 1);
    rewind (stream);
    fread (license, sizeof (gchar), size, stream);
    license[size] = '\0';

    gtk_text_buffer_set_text (self->textbuffer1, license, -1);
    set_center_line (self, 1);
    g_free (license);
  }
}