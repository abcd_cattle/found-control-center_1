/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2019 Purism SPC
 * Copyright (C) 2017 Mohammed Sadiq <sadiq@sadiqpk.org>
 * Copyright (C) 2010 Red Hat, Inc
 * Copyright (C) 2008 William Jon McCann <jmccann@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n.h>

#include "cc-info-contact-panel.h"
#include "cc-info-contact-resources.h"

struct _CcInfoContactPanel
{
  CcPanel     parent_instance;
};

G_DEFINE_TYPE (CcInfoContactPanel, cc_info_contact_panel, CC_TYPE_PANEL)

static void
cc_info_contact_panel_finalize (GObject *object)
{
  CcInfoContactPanel *self = CC_INFO_CONTACT_PANEL (object);
  
  G_OBJECT_CLASS (cc_info_contact_panel_parent_class)->finalize (object);
}

static void
cc_info_contact_panel_dispose (GObject *object)
{
  CcInfoContactPanel *self = CC_INFO_CONTACT_PANEL (object);

  G_OBJECT_CLASS (cc_info_contact_panel_parent_class)->dispose (object);
}

static void
cc_info_contact_panel_class_init (CcInfoContactPanelClass *klass)
{
  GObjectClass    *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass  *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = cc_info_contact_panel_finalize;
  object_class->dispose = cc_info_contact_panel_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/control-center/info-contact/cc-info-contact-panel.ui");
}

static void
cc_info_contact_panel_init (CcInfoContactPanel *self)
{
  g_resources_register (cc_info_contact_get_resource ());
  gtk_widget_init_template (GTK_WIDGET (self));
}

